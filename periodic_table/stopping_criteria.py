def check_single_letter(letter, symbols_set, return_bool=True):
    if letter in symbols_set:
        if return_bool:
            return True
        else:
            return letter
    else:
        raise ValueError(f"{letter} is not in the set of possible symbols")


def check_letter_pair(letter_pair, symbols_set, return_bool=True):
    if len(letter_pair) != 2:
        raise ValueError(f"{letter_pair} must be 2 letters long")
    if letter_pair in symbols_set:
        if return_bool:
            return True
        else:
            return letter_pair
    else:
        try:
            for letter in letter_pair:
                check_single_letter(letter, symbols_set)
            if return_bool:
                return True
            else:
                return [letter for letter in letter_pair]
        except:
            raise ValueError(f"{letter_pair} is not in the set of possible symbols")
