from periodic_table.stopping_criteria import check_letter_pair, check_single_letter
from dataclasses import dataclass, field


@dataclass
class LetterSet:

    letters: str
    size: int = None

    def __post_init__(self):
        self.size = len(self.letters)

    def split_letters(self, size_of_first_string):
        split_1 = self.letters[:size_of_first_string]
        split_2 = self.letters[size_of_first_string:]
        return LetterSet(split_1), LetterSet(split_2)


@dataclass
class LetterSetBreakdown:

    list_of_components: list = field(default_factory=lambda: [])

    def add_component(self, component):
        self.list_of_components.append(component)

    @property
    def symbol_list(self):
        return list(self.flatten(self.list_of_components))

    @staticmethod
    def flatten(nested_list):
        for x in nested_list:
            if hasattr(x, "__iter__") and not isinstance(x, str):
                for y in LetterSetBreakdown.flatten(x):
                    yield y
            else:
                yield x


def decompose_word(letterset, symbols_set, letterset_breakdown=LetterSetBreakdown()):
    """If possible to make word from symbols, returns one such list of symbols

    Args:
        letterset (LetterSet): Letters to attempt to split into symbols
        symbols_set (set): Set of possible collections of characters into which the letterset can be split
        letterset_breakdown (LetterSetBreakdown, optional): Current decomposition to get from the initial word
            to the current letterset. Used to carry information through the recusrion loop.
            Default = LetterSetBreakdown()

    Raises:
        ValueError: If impossible to split the letterset into component symbols from symbols_set

    Returns:
        list: List of symbols into which the letterset can be split
    """
    if letterset.size == 1:
        component_symbol = check_single_letter(
            letterset.letters, symbols_set, return_bool=False
        )
        letterset_breakdown.add_component(component_symbol)
        return letterset_breakdown.symbol_list
    elif letterset.size == 2:
        component_symbols = check_letter_pair(
            letterset.letters, symbols_set, return_bool=False
        )
        letterset_breakdown.add_component(component_symbols)
        return letterset_breakdown.symbol_list
    else:
        try:  # First check if taking the first 2 characters can lead to a solution
            return split_letterset(letterset, 2, symbols_set, letterset_breakdown)
        except:
            try:  # If taking 2 characters doesn't work, try just taking the first
                return split_letterset(letterset, 1, symbols_set, letterset_breakdown)
            except:
                raise (
                    ValueError(
                        f"{letterset.letters} cannot be broken down to the symbols in the set provided"
                    )
                )


def split_letterset(letterset, n_characters_in_prefix, symbols_set, letterset_breakdown):
    """ Check if splitting the letterset into two can lead to full breakdown to symbols from symbols_set

    Args:
        letterset (LetterSet): Letters to attempt to split into symbols
        n_characters_in_prefix (int): Number of characters to split away from the start of the letterset
        symbols_set (set): Set of possible collections of characters into which the letterset can be split
        letterset_breakdown (LetterSetBreakdown, optional): Current decomposition to get from the initial word
            to the current letterset. Used to carry information through the recusrion loop.
            Default = LetterSetBreakdown()

    Returns:
        list: List of symbols into which the letterset can be split
    """
    prefix, suffix = letterset.split_letters(n_characters_in_prefix)
    decompose_word(prefix, symbols_set, letterset_breakdown)
    decompose_word(suffix, symbols_set, letterset_breakdown)
    return letterset_breakdown.symbol_list
