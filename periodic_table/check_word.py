from periodic_table.stopping_criteria import check_letter_pair, check_single_letter


def check_word_comprises_symbols_from_set(word, symbols_set):
    """If possible to make word from symbols, returns True, else raises Error"""
    if len(word) == 1:
        return check_single_letter(word, symbols_set)
    elif len(word) == 2:
        return check_letter_pair(word, symbols_set)
    else:
        print(f"Attempting to split into {word[:2]} and {word[2:]}")
        try:
            check_word_comprises_symbols_from_set(word[:2], symbols_set)
            check_word_comprises_symbols_from_set(word[2:], symbols_set)
            return True
        except:
            print(f"Attempting to split into {word[:1]} and {word[1:]}")
            try:
                check_word_comprises_symbols_from_set(word[:1], symbols_set)
                check_word_comprises_symbols_from_set(word[1:], symbols_set)
                return True
            except:
                raise (
                    ValueError(
                        f"{word} cannot be broken down to the symbols in the set provided"
                    )
                )
