from periodic_table.check_word import *
import pytest

class TestCheckWordForImmediateStop:

    def test_check_single_letter_word_returns_true_if_present(self):
        output = check_word_comprises_symbols_from_set('a', {'a','b'})
        assert type(output)==bool
        assert output == True

    def test_check_double_letter_word_returns_true_if_double_present(self):
        output = check_word_comprises_symbols_from_set('ab', {'ab','c'})
        assert type(output)==bool
        assert output == True

    def test_check_double_letter_word_returns_true_if_both_singles_present(self):
        output = check_word_comprises_symbols_from_set('ab', {'a','b'})
        assert type(output)==bool
        assert output == True

    def test_check_double_letter_word_raises_error_if_one_letter_missing(self):
        with pytest.raises(ValueError, match="ab is not in the set of possible symbols"):
            output = check_word_comprises_symbols_from_set('ab', {'a','c'})


class TestCheckLongerWord:

    def test_check_three_letter_word_finds_working_solution(self):
        output = check_word_comprises_symbols_from_set('aab', {'a','b'})
        assert type(output)==bool
        assert output == True

    def test_check_three_letter_word_raises_error_if_not_possible(self):
        with pytest.raises(ValueError, match="abc cannot be broken down to the symbols in the set provided"):
            output = check_word_comprises_symbols_from_set('abc', {'ab','bc'})



        
