from periodic_table.stopping_criteria import *
import pytest

class TestCheckSingleLetter:

    def test_check_single_letter_returns_true_when_ckecking_present_letter(self):
        output = check_single_letter('a', {'a','b'})
        assert type(output) == bool
        assert output == True

    def test_check_single_letter_raises_error_when_letter_not_present(self):
        with pytest.raises(ValueError, match="a is not in the set of possible symbols"):
            check_single_letter('a', {'ab','b'})


class TestCheckDoubleLetter:

    def test_check_letter_pair_returns_true_if_double_is_in_set(self):
        output = check_letter_pair('ab', {'ab','b'})
        assert type(output) == bool
        assert output == True

    def test_check_letter_pair_raises_error_if_double_not_in_set(self):
        with pytest.raises(ValueError, match="ab is not in the set of possible symbols"):
            check_letter_pair('ab', {'a','c'})

    def test_check_letter_pair_finds_single_letters_if_double_not_present(self):
        output = check_letter_pair('ab', {'a','b'})
        assert type(output) == bool
        assert output == True




    
